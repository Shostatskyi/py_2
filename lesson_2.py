orders = []
names = []
items = []
 
food = ['еда', 'пицца', 'молоко', 'сыр', 'яблоки', 'вода', 'сок', 'мясо', 'йогурт']
drags = ['медикаменты', 'лекарства', 'таблетки']
 
while True:
  
  print('Введите цену товара: ')
  order_sum = int(input())
  
  print('Введите количество: ')
  order_num = int(input())
  
  if order_sum and not order_num:
    print('Логическая ошибка: цена есть, а количество равно нулю')
    continue
  
  if not order_sum and order_num:
    print('Логическая ошибка: цены нет, а количество есть')
    continue
  
  if order_sum < 0 or order_num < 0:
    print('Логическая ошибка: цена и количество не могут быть меньше нуля')
    continue
    
  if not order_sum and not order_num:
    break
  
  print('Введите назавние: ')
  order_name = input()
  
  orders.append(order_sum)
  names.append(order_name)
  items.append(order_num)
  
total_order_taxes = 0
 
print('\nТраты с налогами:')
 
for order, name, num in zip(orders, names, items):
  if(name in food):
    tax = 0.02
  elif name in drags:
    tax = 0.0 
  elif name.lower() == 'iphone':
    tax = 0.10
  else:
    tax = 0.04
    
  order_tax = round(order * num * tax, 2)
  total_order_taxes += order_tax 
  
  print('Налог с %s едениц товара "%s" составляет %s $' % (num, name, order_tax))
 
total_orders_cost = sum((x*y for x,y in zip(orders, items)))
 
print('\nОбщая сумма товаров: %s $' % (total_orders_cost))
print('Общая сумма налогов: %s $' % (round(total_order_taxes, 2)))
print('Общая налоговая нагрузка: %s процентов' % (round(total_order_taxes / total_orders_cost, 2)))
 
print('\nСписок дорогих покупок:')
 
for order, name in zip(orders, names):
  if order > 50:
    print(name, ' - ', order, '$')
    
    
    